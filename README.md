# redir.php
Firefox has a tendency to block mixed content, so this script was created to bypass that.

## Usage
Place the script somewhere that is accessible from the web, e.g. in the document root, and put it in a link.

**Example 1:**
```html
<a href="redir.php?url=http://example.com">Link</a>
```
**Example 2:**

```html
<a href="scripts/redir.php?url=http://example.com">Link</a>"
```