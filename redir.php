<?php

	/*
	*	VERSION:	1.1
	*/

	// Check if the url parameter is empty or not
	if(!empty($_GET['url'])){

		// If it's not empty, put it in variable $url
		$url = $_GET['url'];
	}else{

		/*
		 * If it is empty, take the base domain name, and redirect to it's front page.
		 * This is a fall back in case the url parameter is empty
		 */
		$url = 'https://'.$_SERVER['SERVER_NAME'];
		header("Location: $url");
	}

	// All of this checks if $url has an SSL certificate
	$stream = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));
	$read = fopen("$url", "rb", false, $stream);
	$cont = stream_context_get_params($read);
	$var = ($cont["options"]["ssl"]["peer_certificate"]);
	// The result is stored in $ssl
	$ssl = !is_null($var);

	// If the site has an SSL certificate
	if($ssl){
		// Check if https does not exist in $url
		if(!preg_match('/\bhttps\b/',$url)) $url = preg_replace('/^http:/i', 'https:', $url);

	}

	header("Location: $url");
